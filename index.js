const express = require("express"); // Tương tự : import express from "express";
const path = require("path");

// Khởi tạo Express App
const app = express();

const port = 8000;

app.use(express.static(__dirname + "/views/index"))

// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (request, response) => {
    console.log(__dirname);

    response.sendFile(path.join(__dirname + "/views/index/task51.10.html"))
})

app.listen(port, () => {
    console.log(`App listening on port ${port}`);
})